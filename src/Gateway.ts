import AbstractGateway from "../../omnipay-js/src/AbstractGateway";
import { ReadIObject, IObject } from "../../omnipay-js/src/interfaces";
import { gateway, GatewayConstructor } from "../../omnipay-js/src/GatewayFactory";
import CreditCardRequest from './message/CreditCardRequest';
import TransactionReferenceRequest from './message/TransactionReferenceRequest';
import CardReferenceRequest from './message/CardReferenceRequest';
import IClient from "../../omnipay-js/src/http/IClient";

@gateway('Dummy')
export default class Gateway extends AbstractGateway {
	get name() {
		return 'Dummy';
	}
	get defaultParameters(): ReadIObject {
		return {};
	}
	authorize(parameters: IObject = {}) {
		return this._createRequest(CreditCardRequest, parameters);
	}
	purhase(parameters: IObject = {}) {
		return this._createRequest(CreditCardRequest, parameters);
	}
	completeAuthorize(parameters: IObject = {}) {
		return this._createRequest(TransactionReferenceRequest, parameters);
	}
	capture(parameters: IObject = {}) {
		return this._createRequest(TransactionReferenceRequest, parameters);
	}
	completePurchase(parameters: IObject = {}) {
		return this._createRequest(TransactionReferenceRequest, parameters);
	}
	refund(parameters: IObject = {}) {
		return this._createRequest(TransactionReferenceRequest, parameters);
	}
	void(parameters: IObject = {}) {
		return this._createRequest(TransactionReferenceRequest, parameters);
	}
	createCard(parameters: IObject = {}) {
		return this._createRequest(CreditCardRequest, parameters);
	}
	updateCard(parameters: IObject = {}) {
		return this._createRequest(CardReferenceRequest, parameters);
	}
	deleteCard(parameters: IObject = {}) {
		return this._createRequest(CardReferenceRequest, parameters);
	}
}
