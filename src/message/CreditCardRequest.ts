import AbstractRequest from "../../../omnipay-js/src/message/AbstractRequest";
import CreditCard from "../../../omnipay-js/src/CreditCard";
import Response from "./Response";
import IResponse from "../../../omnipay-js/src/message/IResponse";

var uniqid = () => Math.random().toString() + Math.random().toString();

export default class CreditCardRequest extends AbstractRequest {
	get data() {
		this.validate('amount', 'card');
		(this.card as CreditCard).validate();
		return {
			amount: this.amount
		}
	}
	async send(): Promise<IResponse> {
		var data = this.data;
		var card = this.card as CreditCard;
		var success = parseInt(card.number.substr(-1, 1)) % 2 === 0;
		Object.assign(data, {
			reference: uniqid(),
			success,
			message: success ? 'Success' : 'Failure'
		});
		return this._response = new Response(this, data);
	}
}