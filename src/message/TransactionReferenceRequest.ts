import { ReadIObject, IObject } from "../../../omnipay-js/src/interfaces";
import AbstractRequest from "../../../omnipay-js/src/message/AbstractRequest";
import IResponse from "../../../omnipay-js/src/message/IResponse";
import Response from "./Response";

export default class TransactionReferenceRequest extends AbstractRequest {
	get data() {
		this.validate('transactionReference');
		return {
			transactionReference: this.transactionReference
		};
	}
	async send(): Promise<IResponse> {
		var data = this.data;
		var success = !this.transactionReference.search('fail');
		Object.assign(data, {
			reference: this.transactionReference,
			success,
			message: success ? 'Success' : 'Failure'
		});
		return new Response(this, data);
	}
}