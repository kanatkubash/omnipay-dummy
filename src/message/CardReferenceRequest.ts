import AbstractRequest from "../../../omnipay-js/src/message/AbstractRequest";
import IResponse from "../../../omnipay-js/src/message/IResponse";
import { ReadIObject, IObject } from "../../../omnipay-js/src/interfaces";
import Response from './Response';

export default class CardReferenceRequest extends AbstractRequest {
	get data() {
		this.validate('cardReference');
		return { cardReference: this.cardReference };
	}
	async send(): Promise<IResponse> {
		var data: IObject = this.data;
		data['reference'] = this.cardReference;
		data['success'] = parseInt((this.cardReference.substr(-1, 1))) % 2 === 0;
		data['message'] = data['success'] ? 'Success' : 'Failure';
		return this._response = new Response(this, data);
	}
}