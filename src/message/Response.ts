import AbstractResponse from "../../../omnipay-js/src/message/AbstractResponse";
import { ReadIObject, IObject } from "../../../omnipay-js/src/interfaces";

export default class Response extends AbstractResponse {
	get isSuccesful() {
		return this.data['success'];
	}
	get transactionRefernce() {
		return this.data['reference'] || null;
	}
	isPending: boolean;
	isRedirect: boolean;
	isTransparentRedirect: boolean;
	isCancelled: boolean;
	get message() {
		return this.data['message'] || null;
	}
	code: string;
	transactionReference: string;
	get transactionId() {
		return this.data['reference'] || null;
	}
	get cardReference(): string {
		return this.data['reference'] || null;
	}
	redirectUrl: string;
	redirectMethod: string;
	redirectData: IObject;
}