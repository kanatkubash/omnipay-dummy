import Omnipay from "../omnipay-js/src/Omnipay";
import Gateway from './src/Gateway';
import IClient from "../omnipay-js/src/http/IClient";
///Without this, metadata is not generated , therefore gateway not found
import './src/Gateway';
import IHttpResponse from "../omnipay-js/src/http/IHttpResponse";

class MoqClient implements IClient {
	send(method: "GET" | "POST", uri: string, headers: Object, body: Object): IHttpResponse {
		throw new Error("Method not implemented.");
	}
	get(uri: string, headers: Object, queryObj: Object): IHttpResponse {
		throw new Error("Method not implemented.");
	}
	post(uri: string, headers: Object, postBody: Object): IHttpResponse {
		throw new Error("Method not implemented.");
	}
}

(async () => {
	Omnipay.client = new MoqClient;
	var gateway = Omnipay.create<Gateway>('Dummy');
	var authReq = gateway.authorize({
		card: {
			number: '6011000990139424',
			expiryMonth: '12',
			expiryYear: '2018'
		},
		amount: '123.45'
	});
	try {
		var result = await authReq.send();
		console.log(result.isSuccesful);
	}
	catch (e) {
		console.log(e);
	}
})()
